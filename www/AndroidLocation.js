var exec = cordova.require('cordova/exec');

var AndroidLocation = function() {
    console.log('AndroidLocation instanced');
};

AndroidLocation.prototype.getLocationFromAndroid = function(msg, onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };

    exec(successCallback, errorCallback, 'AndroidLocation', 'getLocationFromAndroid', [msg]);
};

if (typeof module != 'undefined' && module.exports) {
    module.exports = AndroidLocation;
}
# Cordova Android Location plugin

## TL;DR
This is a Cordova plugin for Android that get location latitude and longitude from native location service.

## Using the plugin
First, you need to install it. If you're using Ionic:

`ionic cordova plugin add android-location-plugin`

If you're using Cordova:

`cordova plugin add android-location-plugin`

If you're hitting error "Failed to install 'cordova-android-location': Error: spawn EACCES", run: 
`chmod +x platforms/android/gradlew`

In the code, you would show the toast message like this:

```
constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
        platform.ready().then(() => {
            var AndroidLocation = new AndroidLocation();
            AndroidLocation.show(
                'Test location popup!',
                function(msg) {
                    console.log(msg);
                },
                function(err) {
                    console.log(err);
                }
            );

        });
    }
```

If you're using the newest (currently 3) version of Ionic, then you would have to add this line: `declare var AndroidLocation: any;` after the imports in `app.component.ts` (or any other file where you'll use the plugin) before actually using it. If you're using Ionic 1, you don't need to do this.

As a final note, make sure you're accessing the plugin after the `platform.ready()` fires, just so you make sure that the plugin is ready for use.